/* eslint-disable @typescript-eslint/naming-convention */
import Vue from 'vue';
import { IAPI } from '~/plugins/api/API';

declare module '*.vue' {
  export default Vue;
}
declare module 'vue/types/vue' {
  interface Vue {
    $API: IAPI,
  }
}
declare module '@nuxt/types' {
  interface Context {
    $API: IAPI,
  }
}

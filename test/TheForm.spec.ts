import { mount } from '@vue/test-utils';
import Vue from 'vue';
// @ts-ignore для импортируемого компонента, нужен для того чтобы ts не ругался на него. ну не дружат ts и vue че поделать... а писать под каждый компонент свой интерфейс ну такое...
import { extend } from 'vee-validate';
// import { required } from 'vee-validate/dist/rules';
import flushPromises from 'flush-promises';
// @ts-ignore
import TaskPage from '~/pages/task.vue';
extend('required', jest.fn());
describe('TaskPage', () => {
  let wrapper: any;
  function createComponent () {
    wrapper = mount(TaskPage);
  }
  // afterEach(() => {
  //   wrapper.destroy();
  // });
  it('is a Vue instance', () => {
    const wrapper = mount(TaskPage, { propsData: {} });
    expect(wrapper.vm).toBeTruthy();
  });

  it('should click button', async () => {
    const wrapper = mount(TaskPage, { propsData: {} });
    await wrapper.find('button').trigger('click');
  });

  it('checks if all inputs exists', async () => {
    createComponent();
    const nameInput = wrapper.find('[data-testid="name"]');
    const surnameInput = wrapper.find('[data-testid="surname"]');
    const iinInput = wrapper.find(' [data-testid="iin"]');

    await Vue.nextTick();
    expect(nameInput.exists()).toBe(true);
    expect(surnameInput.exists()).toBe(true);
    expect(iinInput.exists()).toBe(true);
  });
  it('checks vee validate', async () => {
    const wrapper = mount(TaskPage);

    const textInput = wrapper.find('.input');
    const error = wrapper.find('.error');
    // await textInput.setValue('some value1');
    (textInput.element as HTMLInputElement).value = 'some value 1';

    textInput.trigger('input');
    await flushPromises();
    console.log('check', textInput.html());
    // const error = wrapper.vm.$refs.provider.errors[0];
    expect(error).toBeTruthy();
    // expect(error.text()).toBe(
    //   'This field should consist only alphabetical characters ',
    // );
  });
});
// console.log(1, input.element, 1);

// console.log(1, input.element, '4INA WRAPPER', wrapper.vm.form, 2);
// expect(wrapper.vm.form.name).toBe('somevalue');
// await input.setValue('somevalue1');
// input.trigger('input');
// expect(input.text()).toBe('somevalue');
// expect(error.exists()).toBe(true);
// expect(error.text()).toBe(
//   'This field should consist only alphabetical characters '
// );

import { mount } from '@vue/test-utils';
// import Vue from 'vue';
import { extend } from 'vee-validate';
// import { required } from 'vee-validate/dist/rules';
// @ts-ignore для импортируемого компонента, нужен для того чтобы ts не ругался на него. ну не дружат ts и vue че поделать... а писать под каждый компонент свой интерфейс ну такое...
import TheInput from '~/components/TheInput.vue';
// import { IMenuItem } from '~/components/layout/type';
extend('required', jest.fn());

const props: Array<String> = [];

describe('TheInput', () => {
  it('is a Vue instance', () => {
    const wrapper = mount(TheInput, { propsData: { props } });
    expect(wrapper.vm).toBeTruthy();
  });
  it('should render Foo, then hide it', () => {
    const wrapper = mount(TheInput, { propsData: { props } });
    expect(wrapper.text()).toMatch('');
  });
});

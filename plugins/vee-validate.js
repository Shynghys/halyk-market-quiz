import {
  extend,
} from 'vee-validate';
import {
  required,
  alpha,
} from 'vee-validate/dist/rules';
extend('required', {
  ...required,
  message: 'This field is required ',
});
extend('alpha', {
  ...alpha,
  message: 'This field should consist only alphabetical characters ',
});
extend('is_iin', {
  validate: function checkIIN (iin, __offset /* Внутренний параметр. Игнорируйте его */) {
    const IIN_LENGTH = 12; // Длина ИИНа

    if (typeof iin !== 'string' || iin.length !== IIN_LENGTH) {
      return {
        valid: false,
        data: {
          reason: 'LENGTH_12',
        },
      };
    }

    __offset = (__offset ? Number(__offset) : 0);

    let checkSum = 0;

    for (let i = 0; i < (IIN_LENGTH - 1); ++i) {
      const weight = 1 + ((__offset + i) % IIN_LENGTH);
      checkSum += Number(iin.charAt(i)) * weight;
    }

    checkSum %= IIN_LENGTH - 1;

    if (checkSum === 10 && __offset === 0) {
      return checkIIN(iin, 2);
    }

    return Number(iin.charAt(IIN_LENGTH - 1)) === checkSum;
  },

  message: (field, values) => {
    console.log(field);
    if (values.reason === 'LENGTH_12') {
      return 'IIN should contain 12 characters.';
    }
    return 'Wrong IIN.';
  },
});
